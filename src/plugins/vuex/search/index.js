import state from '@/plugins/vuex/search/state'

import * as getters from '@/plugins/vuex/search/getters'

export default {
    namespaced: true,
    state,
    getters
}
